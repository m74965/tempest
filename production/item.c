#include "item.h"
#include "game.h"
#include "enemy.h"
#include "geometry.h"

Token create_item(const int first_index, const int last_index, const SDL_Point* start_polygon, const SDL_Point* end_polygon)
{
	Token e;
	SDL_Point middle_start_segment;
	middle_start_segment.x = (start_polygon[first_index].x + start_polygon[last_index].x) / 2;
	middle_start_segment.y = (start_polygon[first_index].y + start_polygon[last_index].y) / 2;

	e.start = middle_start_segment;
	e.actual = e.start;

	middle_start_segment.x = (end_polygon[first_index].x + end_polygon[last_index].x) / 2;
	middle_start_segment.y = (end_polygon[first_index].y + end_polygon[last_index].y) / 2;

	e.end = middle_start_segment;

	e.coef = ((double)e.end.y - (double)e.start.y) / ((double)e.end.x - (double)e.start.x);
	e.k = (double)e.start.y - (e.coef * (double)e.start.x);
	e.is_dead = false;
	return e;
}

void move_item(Token* t, int indexEnemy)
{
	if (!t->is_dead) {
		if (t->start.x > t->end.x)
		{
			if (t->actual.x - 1 < t->end.x)
			{
				t->is_dead = true;
				if (t->is_enemy)
				{
					create_enemy(indexEnemy);
					tempest_game.nb_life--;
				}
			}
			else
			{
				t->actual.x--;
			}
		}
		else
		{
			if (t->actual.x + 1 > t->end.x)
			{
				t->is_dead = true;
				if (t->is_enemy)
				{
					create_enemy(indexEnemy);
					tempest_game.nb_life--;
				}
			}
			else
			{
				t->actual.x++;
			}
		}
		t->actual.y = (t->coef * t->actual.x) + t->k;
		bullet_enemy_crash();
	}
}

void create_bullet()
{
	const Uint8* shot = SDL_GetKeyboardState(NULL);

	if (shot[SDL_SCANCODE_SPACE])
	{
		bool quit = false;
		for (int i = 0; i < tempest_game.player.nb_bullets && !quit; i++)
		{
			if (tempest_game.player.bullets[i].is_dead)
			{
				tempest_game.player.bullets[i] = create_item(tempest_game.player.index_first_point, tempest_game.player.index_last_point, tempest_game.big_polygon, tempest_game.small_polygon);
				tempest_game.player.bullets[i].is_enemy = false;
				quit = true;
			}
		}

		if (!quit) {
			tempest_game.player.bullets[tempest_game.player.nb_bullets] = create_item(tempest_game.player.index_first_point, tempest_game.player.index_last_point, tempest_game.big_polygon, tempest_game.small_polygon);
			tempest_game.player.bullets[tempest_game.player.nb_bullets].is_enemy = false;
			tempest_game.player.nb_bullets++;
		}
	}

}

Token create_itemV2(Token enemy) {
	Token bullet = enemy;
	
	return bullet;
		

}

void move_bullet()
{
	for (int i = 0; i < tempest_game.player.nb_bullets; i++)
	{
		if (!tempest_game.player.bullets[i].is_dead) {
			SDL_SetRenderDrawColor(tempest_game.renderer, 255, 0, 0, 255);
			SDL_RenderDrawCircle(tempest_game.renderer, tempest_game.player.bullets[i].actual.x, tempest_game.player.bullets[i].actual.y, 1);
			SDL_RenderFillCircle(tempest_game.renderer, tempest_game.player.bullets[i].actual.x, tempest_game.player.bullets[i].actual.y, 1);
						
			move_item(&tempest_game.player.bullets[i], -1);
		}
	}
}

