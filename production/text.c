﻿#include "text.h"

#include <stdbool.h>
#include "game.h"
#include "asprintf.h"

bool restartSelected = true;
bool play = true;
void displayScore(int score, int x, int y)
{
	char* textToDisplay;
	asprintf(&textToDisplay, "Score: %d", score);
	displayText(textToDisplay, x, y, 1, 255, 0, 0);
}

void displayLevel(int level, int x, int y)
{
	char* textToDisplay;
	asprintf(&textToDisplay, "Niveau: %d", level);
	displayText(textToDisplay, x, y, 1, 255, 0, 0);
}

void displayLives(int lives, int x, int y)
{
	char* textToDisplay;
	asprintf(&textToDisplay, "Vies: %d", lives);
	displayText(textToDisplay, x, y, 1, 255, 0, 0);
}

int displayButtons()
{
	const Uint8* new_direction = SDL_GetKeyboardState(NULL);
	if (new_direction[SDL_SCANCODE_LEFT]) {
		restartSelected = true;
	}
	if(new_direction[SDL_SCANCODE_RIGHT])
	{
		restartSelected = false;
	}
	if(new_direction[SDL_SCANCODE_RETURN])
	{
		if(restartSelected)
		{
			return 1;
		}
			return 0;
		
	}
	if(restartSelected)
	{
		displayText("REJOUER", SCREEN_WIDTH / 8 +100, SCREEN_HEIGHT / 2 + 200, 1, 0, 255, 0);
		displayText("QUITTER", SCREEN_WIDTH / 2+100, SCREEN_HEIGHT / 2 +200, 1, 255, 0, 0);
	}
	else
	{
		displayText("REJOUER", SCREEN_WIDTH / 8+100, SCREEN_HEIGHT / 2+200, 1, 255, 0, 0);
		displayText("QUITTER", SCREEN_WIDTH / 2+100, SCREEN_HEIGHT / 2+200, 1, 0, 255, 0);

	}
	return -1;
}

int displayMenu()
{
	int quit = -1;
	SDL_Event event;
	while (quit == -1 && SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			quit = 0;
			break;
		default: break;
		}
	}

	SDL_SetRenderDrawColor(tempest_game.renderer, 0, 0, 0, 255); // Draw background
	SDL_RenderClear(tempest_game.renderer); // Clean background
	SDL_Delay(100);

	displayText("TEMPEST", 70, 100, 3, 255, 0, 0);
	const Uint8* new_direction = SDL_GetKeyboardState(NULL);
	if (new_direction[SDL_SCANCODE_LEFT]) {
		play=true;
	}
	if (new_direction[SDL_SCANCODE_RIGHT])
	{
		play = false;
	}
	if (new_direction[SDL_SCANCODE_RETURN])
	{
		if (play)
		{
			return 1;
		}
		return 0;

	}
	if (play)
	{
		displayText("JOUER", SCREEN_WIDTH / 8 + 100, SCREEN_HEIGHT / 2 + 200, 1, 0, 255, 0);
		displayText("QUITTER", SCREEN_WIDTH / 2 + 100, SCREEN_HEIGHT / 2 + 200, 1, 255, 0, 0);
	}
	else
	{
		displayText("JOUER", SCREEN_WIDTH / 8 + 100, SCREEN_HEIGHT / 2 + 200, 1, 255, 0, 0);
		displayText("QUITTER", SCREEN_WIDTH / 2 + 100, SCREEN_HEIGHT / 2 + 200, 1, 0, 255, 0);

	}
	SDL_RenderPresent(tempest_game.renderer); // Update	
	return quit;
}



