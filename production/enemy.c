#include "enemy.h"

#include <time.h>

#include "game.h"
#include "geometry.h"



int is_enemy_in_zone(const Token b)
{
	for (int i = 0; i < tempest_game.nb_enemy; i++)
	{
		if (tempest_game.list_enemy[i].end.x == b.start.x && tempest_game.list_enemy[i].end.y == b.start.y && !tempest_game.list_enemy[i].is_dead)
		{
			return i;
		}
	}
	return -1;
}

int is_bullet_enemy_in_zone(const Token b)
{
	for (int i = 0; i < tempest_game.nb_enemy; i++)
	{
		if (tempest_game.list_bullet_enemy[i].end.x == b.start.x && tempest_game.list_bullet_enemy[i].end.y == b.start.y && !tempest_game.list_bullet_enemy[i].is_dead)
		{
			return i;
		}
	}
	return -1;
}

int is_enemy_between_index(const int first_index, const int last_index)
{
	const int start_x = (tempest_game.small_polygon[first_index].x + tempest_game.small_polygon[last_index].x) / 2;
	const int start_y = (tempest_game.small_polygon[first_index].y + tempest_game.small_polygon[last_index].y) / 2;

	for (int i = 0; i < tempest_game.nb_enemy; i++)
	{
		if (tempest_game.list_enemy[i].start.x == start_x && tempest_game.list_enemy[i].start.y == start_y && !tempest_game.list_enemy[i].is_dead)
		{
			return i;
		}
	}
	return -1;
}

void create_enemy(const int index_enemy)
{
	srand(time(0));
	int first_point_index = rand() % (nb_point);
	int last_point_index;
	if (first_point_index == 0) {
		last_point_index = nb_point - 1;
	}
	else {
		last_point_index = first_point_index - 1;
	}

	bool stop = false;
	int count = 1;

	while (!stop && count != nb_point)
	{
		first_point_index++;
		last_point_index++;
		count++;
		if (first_point_index >= nb_point)
		{
			first_point_index = 0;
			last_point_index = nb_point - 1;
		}
		stop = is_enemy_between_index(first_point_index, last_point_index) == -1;
	}
	if (count != nb_point) {
		if (index_enemy == -1) {
			tempest_game.list_enemy[tempest_game.nb_enemy] = create_item(first_point_index, last_point_index, tempest_game.small_polygon, tempest_game.big_polygon);
			tempest_game.list_enemy[tempest_game.nb_enemy].is_enemy = true;
			create_bullet_enemy(&tempest_game.list_enemy[tempest_game.nb_enemy], tempest_game.nb_enemy, -1);
			//tempest_game.list_enemy[tempest_game.nb_enemy].shape = create_polygon(5, tempest_game.list_enemy[tempest_game.nb_enemy].actual.x, tempest_game.list_enemy[tempest_game.nb_enemy].actual.y);
			tempest_game.nb_enemy++;
		}
		else
		{
			tempest_game.list_enemy[index_enemy] = create_item(first_point_index, last_point_index, tempest_game.small_polygon, tempest_game.big_polygon);
			tempest_game.list_enemy[index_enemy].is_enemy = true;
			create_bullet_enemy(&tempest_game.list_enemy[index_enemy], index_enemy, index_enemy);
		}

	}
	
}

void bullet_enemy_crash() {
	for (int i = 0; i < tempest_game.player.nb_bullets; i++)
	{
		const int index_enemy = is_enemy_in_zone(tempest_game.player.bullets[i]);
		const int index_bullet_enemy = is_bullet_enemy_in_zone(tempest_game.player.bullets[i]);

		if (index_enemy != -1 && tempest_game.list_enemy[index_enemy].actual.x == tempest_game.player.bullets[i].actual.x && !tempest_game.list_enemy[index_enemy].is_dead &&
			tempest_game.player.bullets[i].actual.y == tempest_game.list_enemy[index_enemy].actual.y && !tempest_game.player.bullets[i].is_dead)
		{
			tempest_game.player.bullets[i].is_dead = true;
			tempest_game.list_enemy[index_enemy].is_dead = true;
			create_enemy(index_enemy);
			tempest_game.score += 20;
		}

		if (index_bullet_enemy != -1 && tempest_game.list_bullet_enemy[index_bullet_enemy].actual.x == tempest_game.player.bullets[i].actual.x && !tempest_game.list_bullet_enemy[index_bullet_enemy].is_dead &&
			tempest_game.player.bullets[i].actual.y == tempest_game.list_bullet_enemy[index_bullet_enemy].actual.y && !tempest_game.player.bullets[i].is_dead)
		{
			tempest_game.player.bullets[i].is_dead = true;
			tempest_game.list_bullet_enemy[index_bullet_enemy].is_dead = true;
			tempest_game.score += 5;
		}
	}
}

void move_enemy()
{
	for (int i = 0; i < tempest_game.nb_enemy; i++)
	{
		if (!tempest_game.list_enemy[i].is_dead) {

			tempest_game.list_enemy[i].shape = create_polygon(5, tempest_game.list_enemy[i].actual.x, tempest_game.list_enemy[i].actual.y);
			draw_polygon(tempest_game.list_enemy[i].shape, 255, 0, 255);
			move_item(&tempest_game.list_enemy[i], i);
		}
	}

}


void create_bullet_enemy(Token* t, int index, int isNew)
{
	Token tokenBullet = create_itemV2(*t);
	if(isNew==-1)
	{		
		tempest_game.list_bullet_enemy[tempest_game.nb_enemy] = tokenBullet;
	}
	else
	{
		tempest_game.list_bullet_enemy[index] = tokenBullet;
	}
	
	

}

void draw_bullet_enemy() {
	for (int i = 0; i < tempest_game.nb_enemy; i++)
	{
		if (!tempest_game.list_bullet_enemy[i].is_dead) {
			SDL_Point* bulletPointEnemy = create_polygon(2, tempest_game.list_bullet_enemy[i].actual.x, tempest_game.list_bullet_enemy[i].actual.y);
			tempest_game.list_bullet_enemy[i].shape = bulletPointEnemy;
			draw_polygon(tempest_game.list_bullet_enemy[i].shape, 0, 0, 255);
			move_bullet_enemy(&tempest_game.list_bullet_enemy[i]);
		}
	}
}

void move_bullet_enemy(Token* t)
{
	if (!t->is_dead) {
		if (t->start.x > t->end.x)
		{

			if (t->actual.x - 1 < t->end.x)
			{
				t->is_dead = true;
				if (t->is_enemy)
				{
					tempest_game.score = tempest_game.score - 5;
				}
			}
			else
			{
				t->actual.x = t->actual.x - 2;
			}
		}
		else
		{
			if (t->actual.x - 1 < t->end.x)
			{
				t->is_dead = true;
				if (t->is_enemy)
				{
					tempest_game.score = tempest_game.score - 5;
				}
			}
			else
			{
				t->actual.x = t->actual.x + 2;
			}
		}
		t->actual.y = (t->coef * t->actual.x) + t->k;
		bullet_enemy_crash();

	}
}