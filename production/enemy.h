#include <SDL.h>
#include "item.h"

#ifndef ENEMY_H_
#define ENEMY_H_


int is_enemy_in_zone(const Token b);
int is_bullet_enemy_in_zone(const Token b);
int is_enemy_between_index(const int first_index, const int last_index);
void create_enemy(const int index_enemy);
void bullet_enemy_crash();
void move_enemy();
void create_bullet_enemy(Token* t, int index, int isNew);
void move_bullet_enemy(Token* t);
void draw_bullet_enemy();


#endif