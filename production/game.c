#include "game.h"
#include "enemy.h"
#include "geometry.h"
#include "hershey.h"
#include "text.h"

int nb_point = 4;
int score_next_round = 0;
int nb_enemy_max = 1;
int nb_bullets_max = 50;

int play_round()
{
	int quit = -1;
	while (quit==-1 && tempest_game.score < score_next_round)
	{
		SDL_Event event;
		while (quit==-1 && SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = 0;
				break;
			default: break;
			}
		}

		SDL_SetRenderDrawColor(tempest_game.renderer, 0, 0, 0, 255); // Draw background
		SDL_RenderClear(tempest_game.renderer); // Clean background
		SDL_Delay(100);
			
		
		displayScore(tempest_game.score, 10, 60);
		displayLevel(tempest_game.level, 10, 30);
		displayLives(tempest_game.nb_life, 650, 30);

		if(tempest_game.nb_life!=0)
		{
			if (tempest_game.nb_enemy < nb_enemy_max)
			{
				create_enemy(-1);
			}
			// draw and move items
			draw_polygon(tempest_game.small_polygon, 255, 102, 0);
			draw_polygon(tempest_game.big_polygon, 255, 102, 0);
			draw_link_polygon();
			draw_player();
			create_bullet();
			draw_bullet_enemy();
			move_player();
			move_bullet();
			move_enemy();
		}
		else
		{
			displayText("GAME OVER", SCREEN_WIDTH / 6 - 20, SCREEN_HEIGHT / 4, 2, 255, 0, 0);
			int res = displayButtons();
			if(res!=-1)
			{
				quit = res;
				nb_point = 4;
				score_next_round = 0;
				nb_enemy_max = 1;
				nb_bullets_max = 50;
			}
		}

		SDL_RenderPresent(tempest_game.renderer); // Update	

	}

	// free round items
	free(tempest_game.big_polygon);
	free(tempest_game.small_polygon);
	free(tempest_game.list_enemy);
	free(tempest_game.player.bullets);
	free(tempest_game.list_bullet_enemy);


	return quit;
	
}

void init_round()
{
	// center screen
	const float center_x = SCREEN_WIDTH / 2.0;
	const float center_y = SCREEN_HEIGHT / 2.0;

	// param next round
	if(tempest_game.level%2 == 0)
	{
		if (nb_point <= 15) {
			nb_point += 1;
		}
		score_next_round += 100;

	}
	else
	{
		if (nb_enemy_max <= 4) {
			nb_enemy_max += 1;
		}
		score_next_round += 200;
	}

	// create and init items
	tempest_game.small_polygon = create_polygon(10, center_x + 50, center_y + 60);
	tempest_game.big_polygon = create_polygon(200, center_x, center_y);
	tempest_game.nb_enemy = 0;
	tempest_game.list_enemy =(Token*) malloc(sizeof(Token) * nb_enemy_max);
	tempest_game.list_bullet_enemy = (Token*)malloc(sizeof(Token) * nb_enemy_max);
	tempest_game.player.bullets =(Token*) malloc(sizeof(Token) * nb_bullets_max);
	tempest_game.player.nb_bullets = 0;

	init_player_position();

}

