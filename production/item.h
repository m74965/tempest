#include <SDL.h>
#include <stdbool.h>
#ifndef ITEM_H_
#define ITEM_H_

typedef struct
{
	SDL_Point start;
	SDL_Point end;
	SDL_Point actual;
	SDL_Point* shape;
	double coef;
	double k;
	bool is_dead;
	bool is_enemy;
} Token;

Token create_item(const int first_index, const int last_index, const SDL_Point* start_polygon, const SDL_Point* end_polygon);
void move_item(Token* t, int indexEnemy);

void create_bullet();
void move_bullet();
Token create_itemV2(Token enemy);


#endif