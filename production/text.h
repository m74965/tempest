#include <SDL.h>
#include "hershey.h"

#ifndef TEXT_H_
#define TEXT_H_

void displayScore(int score, int x, int y);
void displayLevel(int level, int x, int y);
void displayLives(int lives, int x, int y);
int displayButtons();
int displayMenu();
#endif
