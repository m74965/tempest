#include "SDL_render.h"
#include <string.h>

#ifndef TEST_SDL_HERSHEY_H
#define TEST_SDL_HERSHEY_H

void displayText(char* str, int x, int y, int size, int red, int green, int blue);

#endif //TEST_SDL_HERSHEY_H
