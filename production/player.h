#include <SDL.h>
#include "item.h"
#ifndef PLAYER_H_
#define PLAYER_H_



typedef struct {
	int index_first_point;
	int index_last_point;
	int nb_bullets;
	Token* bullets;
} PositionPlayer;

void init_player_position();
void draw_player();
void move_player();
#endif