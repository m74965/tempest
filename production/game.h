#include <SDL.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include "player.h"

#ifndef TP_SDL_H_  /* Include guard */
#define TP_SDL_H_

#define SCREEN_WIDTH   800
#define SCREEN_HEIGHT  600

extern SDL_Surface* spritesASCII;

extern int nb_point;
extern int score_next_round;
extern int nb_enemy_max;
extern int nb_bullets_max ;



typedef struct {
	SDL_Renderer* renderer;
	SDL_Window* window;
	PositionPlayer player;
	SDL_Point* small_polygon;
	SDL_Point* big_polygon;
	int score;
	int level;
	Token* list_enemy;
	Token* list_bullet_enemy;
	int nb_life;
	int nb_enemy;
} Tempest_Game;


Tempest_Game tempest_game;

int play_round();
void init_round();

#endif