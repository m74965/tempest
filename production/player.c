#include "player.h"
#include "game.h"


void init_player_position()
{
	tempest_game.player.index_first_point = 0;
	tempest_game.player.index_last_point = 1;
}

void draw_player()
{
	const int first_index = tempest_game.player.index_first_point;
	const int last_index = tempest_game.player.index_last_point;
	SDL_SetRenderDrawColor(tempest_game.renderer, 0, 128, 0, 255);
	SDL_RenderDrawLine(tempest_game.renderer, tempest_game.big_polygon[first_index].x, tempest_game.big_polygon[first_index].y,
		tempest_game.big_polygon[last_index].x, tempest_game.big_polygon[last_index].y);
	SDL_RenderDrawLine(tempest_game.renderer, tempest_game.big_polygon[first_index].x, tempest_game.big_polygon[first_index].y,
		tempest_game.small_polygon[first_index].x, tempest_game.small_polygon[first_index].y);
	SDL_RenderDrawLine(tempest_game.renderer, tempest_game.big_polygon[last_index].x, tempest_game.big_polygon[last_index].y,
		tempest_game.small_polygon[last_index].x, tempest_game.small_polygon[last_index].y);
}

void move_player()
{
	const Uint8* new_direction = SDL_GetKeyboardState(NULL);
	// LEFT
	if (new_direction[SDL_SCANCODE_LEFT]) {
		if (tempest_game.player.index_last_point == nb_point - 1) {
			tempest_game.player.index_last_point = 0;
			tempest_game.player.index_first_point++;
		}
		else if (tempest_game.player.index_first_point == nb_point - 1)
		{
			tempest_game.player.index_first_point = 0;
			tempest_game.player.index_last_point++;
		}
		else
		{
			tempest_game.player.index_first_point++;
			tempest_game.player.index_last_point++;
		}
		return;
	}
	// RIGHT
	if (new_direction[SDL_SCANCODE_RIGHT]) {
		if (tempest_game.player.index_last_point == 0) {
			tempest_game.player.index_last_point = nb_point - 1;
			tempest_game.player.index_first_point--;
		}
		else if (tempest_game.player.index_first_point == 0)
		{
			tempest_game.player.index_first_point = nb_point - 1;
			tempest_game.player.index_last_point--;
		}
		else
		{
			tempest_game.player.index_first_point--;
			tempest_game.player.index_last_point--;
		}
	}
}