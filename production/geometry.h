#include <SDL.h>
#ifndef GEOMETRY_H_
#define GEOMETRY_H_

SDL_Point* create_polygon(int radius, float centerX, float centerY);
void draw_polygon(const SDL_Point* point, Uint8 red, Uint8 green, Uint8 blue);
void draw_link_polygon();
int SDL_RenderDrawCircle(SDL_Renderer* renderer, int x, int y, int radius);
int SDL_RenderFillCircle(SDL_Renderer* renderer, int x, int y, int radius);

#endif