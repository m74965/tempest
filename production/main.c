#include <assert.h>
#include <stdio.h>
#include "text.h"
#include "game.h"


int main(int argc, char** argv)
{
	// Init SDL
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		fprintf(stderr, "Pb init SDL\n");
		return 0;
	}

	// Create SDL window
	tempest_game.window = SDL_CreateWindow("Tempest", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);
	tempest_game.renderer = SDL_CreateRenderer(tempest_game.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);


	assert(tempest_game.renderer != NULL);

	// Init and Play round 
	int quit = -1;
	tempest_game.level = 0;
	tempest_game.nb_life = 3;
	bool menuDisplay = true;
	while (quit!=0) {
		if(menuDisplay)
		{
			
			int res = displayMenu();
			if(res == 0)
			{
				quit = res;
			}
			if(res == 1)
			{
				menuDisplay = false;
			}
		}
		else {
			init_round();
			quit = play_round();
			tempest_game.level++;
			if (quit == 1)
			{
				tempest_game.level = 0;
				tempest_game.nb_life = 3;
				tempest_game.score = 0;
			}
		}
	}

	// Quit SDL
	SDL_Quit();

	return 0;
}

