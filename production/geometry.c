#include "geometry.h"
#include "game.h"

SDL_Point* create_polygon(int radius, float centerX, float centerY)
{
	SDL_Point* point = malloc(nb_point * sizeof(long) * radius);
	for (int i = 0; i <= nb_point; i++)
	{
		if (i + 1 <= nb_point) {
			point[i].x = (int)((double)centerX + radius * cos(((double)i + 1) * 2 * M_PI / nb_point));
			point[i].y = (int)((double)centerY + radius * sin(((double)i + 1) * 2 * M_PI / nb_point));
		}
		else
		{
			point[i].x = (int)((double)centerX + radius * cos(2.0 * M_PI / nb_point));
			point[i].y = (int)((double)centerY + radius * sin(2.0 * M_PI / nb_point));
		}
	}
	return point;
}

void draw_polygon(const SDL_Point* point, Uint8 red, Uint8 green, Uint8 blue, bool fill)
{
	SDL_SetRenderDrawColor(tempest_game.renderer, red, green, blue, 255);
	SDL_RenderDrawLines(tempest_game.renderer, point, nb_point + 1);
	
}

void draw_link_polygon()
{
	for (int i = 0; i < nb_point; i++) {
		SDL_RenderDrawLine(tempest_game.renderer, tempest_game.small_polygon[i].x, tempest_game.small_polygon[i].y,
			tempest_game.big_polygon[i].x, tempest_game.big_polygon[i].y);
	}
}

int SDL_RenderDrawCircle(SDL_Renderer* renderer, int x, int y, int radius)
{
    int offsetx, offsety, d;
    int status;

    offsetx = 0;
    offsety = radius;
    d = radius - 1;
    status = 0;

    while (offsety >= offsetx) {
        status += SDL_RenderDrawPoint(renderer, x + offsetx, y + offsety);
        status += SDL_RenderDrawPoint(renderer, x + offsety, y + offsetx);
        status += SDL_RenderDrawPoint(renderer, x - offsetx, y + offsety);
        status += SDL_RenderDrawPoint(renderer, x - offsety, y + offsetx);
        status += SDL_RenderDrawPoint(renderer, x + offsetx, y - offsety);
        status += SDL_RenderDrawPoint(renderer, x + offsety, y - offsetx);
        status += SDL_RenderDrawPoint(renderer, x - offsetx, y - offsety);
        status += SDL_RenderDrawPoint(renderer, x - offsety, y - offsetx);

        if (status < 0) {
            status = -1;
            break;
        }

        if (d >= 2 * offsetx) {
            d -= 2 * offsetx + 1;
            offsetx += 1;
        }
        else if (d < 2 * (radius - offsety)) {
            d += 2 * offsety - 1;
            offsety -= 1;
        }
        else {
            d += 2 * (offsety - offsetx - 1);
            offsety -= 1;
            offsetx += 1;
        }
    }

    return status;
}


int SDL_RenderFillCircle(SDL_Renderer* renderer, int x, int y, int radius)
{
    int offsetx, offsety, d;
    int status;

    offsetx = 0;
    offsety = radius;
    d = radius - 1;
    status = 0;

    while (offsety >= offsetx) {

        status += SDL_RenderDrawLine(renderer, x - offsety, y + offsetx,
            x + offsety, y + offsetx);
        status += SDL_RenderDrawLine(renderer, x - offsetx, y + offsety,
            x + offsetx, y + offsety);
        status += SDL_RenderDrawLine(renderer, x - offsetx, y - offsety,
            x + offsetx, y - offsety);
        status += SDL_RenderDrawLine(renderer, x - offsety, y - offsetx,
            x + offsety, y - offsetx);

        if (status < 0) {
            status = -1;
            break;
        }

        if (d >= 2 * offsetx) {
            d -= 2 * offsetx + 1;
            offsetx += 1;
        }
        else if (d < 2 * (radius - offsety)) {
            d += 2 * offsety - 1;
            offsety -= 1;
        }
        else {
            d += 2 * (offsety - offsetx - 1);
            offsety -= 1;
            offsetx += 1;
        }
    }

    return status;
}